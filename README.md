# Install

1. Install SPONGE

  See https://spongemm.cn/. SPONGE should be 1.4 for this module

2. Install PLUMED

  a. Download PLUMED from the official website or other places.

  b. Extract the files and `cd` to the extracted folder.

  c. Install PLUMED to a place, here use `~/plumed` as an example.

  ```
  ./configure --prefix=~/plumed
  make -j 24
  make install
  ```

  d. Set the environment variables. For example, `vim ~/.bashrc` and add the following new lines:

  ```
  #FOR PLUMED
  export PLUMED_ROOT=~/plumed
  export PATH=$PATH:$PLUMED_ROOT/bin
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PLUMED_ROOT/lib
  ```
  
3. `cd` to the plugin source folder, and compile PLUMED for SPONGE

  ```bash
  cd src
  make
  ```

Then, a dynamic libarary named `PLUMED_for_SPONGE.so` will be in the folder.

# Usage

In SPONGE `mdin.txt`, add a command:

```
other SPONGE commands
plugin = xxx/xxx/PLUMED_for_SPONGE.so
plumed_in_file = xxx.txt
```

The command `plugin` specifies the dynamic libarary

The command `plumed_in_file` specifies the PLUMED input file (can be used with `default_in_file_prefix`)

The command `plumed_log_file` specifies the PLUMED log file (plumed.log for default)

# Examples

The `unittest1` folder contains an example, in which a well-tempered metadynamics is performed.

```
cd unittest1
SPONGE
python ana.py
```

The `unittest2` folder contains an example, in which the box length in the x direction is restrained to 25 Angstrom

```
cd unittest2
SPONGE
cat c.txt
```
