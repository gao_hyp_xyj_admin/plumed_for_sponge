#ifdef _WIN32
#define PLUGIN_API extern "C" __declspec(dllexport)
#elif __linux__
#define PLUGIN_API extern "C"
#endif

#include "common.cuh"
#include "control.cuh"
#include "collective_variable/collective_variable.cuh"
#include "MD_core/MD_core.cuh"
#include "neighbor_list/neighbor_list.cuh"

#include "plumed/wrapper/Plumed.h"


static MD_INFORMATION* md_info = NULL;
static CONTROLLER* controller = NULL;
static COLLECTIVE_VARIABLE_CONTROLLER* cv_controller = NULL;
static NEIGHBOR_LIST* neighbor_list = NULL;
static PLMD::Plumed *p = NULL;
static int is_initialized = 0;

static __global__ void add_virial(float* toadd, const float adder)
{
    toadd[0] -= adder;
}

PLUGIN_API std::string Name()
{
    return std::string("PLUMED for SPONGE");
}

PLUGIN_API std::string Version()
{
    return std::string("1.4b1");
}

PLUGIN_API std::string Version_Check(int i)
{
    if (i < 20231219)
    {
        return std::string("Reason:\n\tPLUMED for SPONGE v1.4b1 can not be used with SPONGE before 20231219. Your SPONGE: " + std::to_string(i));
    }
    return std::string();
}

PLUGIN_API void Initial(MD_INFORMATION* md, CONTROLLER* ctrl, NEIGHBOR_LIST* nl,
                        COLLECTIVE_VARIABLE_CONTROLLER* cv, CV_MAP_TYPE* cv_map, CV_INSTANCE_TYPE* cv_instance_map)
{
    md_info = md;
    controller = ctrl;
    neighbor_list = nl;
    cv_controller = cv;
    CV_MAP = cv_map;
    CV_INSTANCE_MAP = cv_instance_map;
    controller->printf("    initializing PLUMED for SPONGE\n");
    if (!controller->Command_Exist("plumed", "in_file"))
    {
        controller->printf("        No 'plumed_in_file' command found. PLUMED for SPONGE will not be initialized.\n");
        controller->Warn("        No 'plumed_in_file' command found. PLUMED for SPONGE will not be initialized.\n");
        return;
    }
    is_initialized = 1;
    p = new PLMD::Plumed;
    int api_version = 0;
    p->cmd("getApiVersion", &api_version);
    controller->printf("        PLUMED API version: %d\n", api_version);
    double energyUnits = 4.184;
    double lengthUnits = 0.1;
    p->cmd("setMDEnergyUnits", &energyUnits);
    p->cmd("setMDLengthUnits", &lengthUnits);
    if (controller->Command_Exist("plumed", "log_file"))
    {
        p->cmd("setLogFile", controller->Command("plumed", "log_file"));
    }
    else
    {
        p->cmd("setLogFile", "plumed.log");
    }
    p->cmd("setPlumedDat", controller->Command("plumed", "in_file"));
    p->cmd("setNatoms", &md_info->atom_numbers);
    p->cmd("setMDEngine", "SPONGE");
    int realPrecision = 4;
    p->cmd("setRealPrecision", &realPrecision);
    float temp_dt = md_info->sys.dt_in_ps;
    if (temp_dt == 0)
        temp_dt = 1.0f;
    p->cmd("setTimestep", &temp_dt);
    p->cmd("init");
    controller->Warn("PLUMED_for_SPONGE is only a demo. No strict tests have been performed.\n");
    controller->printf("    end initializing PLUMED for SPONGE\n");
}

PLUGIN_API void After_Initial()
{
    if (!is_initialized)
        return;

}

PLUGIN_API void Calculate_Force()
{
    if (!is_initialized)
        return;
    p->cmd("setStep", &md_info->sys.steps);
    p->cmd("setMasses", &md_info->h_mass[0]);
    cudaMemcpy(md_info->coordinate, md_info->crd, sizeof(VECTOR) * md_info->atom_numbers, cudaMemcpyDeviceToHost);
    cudaMemcpy(md_info->force, md_info->frc, sizeof(VECTOR) * md_info->atom_numbers, cudaMemcpyDeviceToHost);
    p->cmd("setPositions", &md_info->coordinate[0].x);
    p->cmd("setForces", &md_info->force[0].x);
    float virial[3][3];
    float box[3][3];
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            box[i][j] = 0;
            virial[i][j] = 0;
        }
    }
    box[0][0] = md_info->sys.box_length.x;
    box[1][1] = md_info->sys.box_length.y;
    box[2][2] = md_info->sys.box_length.z;
    p->cmd("setVirial", &virial[0]);
    p->cmd("setBox", &box[0][0]);
    p->cmd("prepareCalc");
    p->cmd("performCalc");
    cudaMemcpy(md_info->frc, md_info->force, sizeof(VECTOR) * md_info->atom_numbers, cudaMemcpyHostToDevice);
    add_virial<<<1, 1>>>(md_info->sys.d_virial, virial[0][0] + virial[1][1] + virial[2][2]);
}

PLUGIN_API void Mdout_Print()
{
    if (!is_initialized)
        return;
}
