import numpy as np
import matplotlib.pyplot as plt

positions = np.linspace(-np.pi, np.pi, 360)
theory = 0.3 * np.cos(2 * positions - 0.4) + 0.1 * np.cos(3 * positions + 0.6)
theory *= 50
theory -= min(theory)
plt.plot(positions, theory, label="theory")

data = np.loadtxt("tt.txt")
data[:, 1] = -data[:, 1]
data[:, 1] -= np.min(data[:, 1])
plt.plot(data[:, 0], data[:, 1], label="metad")
plt.legend()
plt.show()
